#!/usr/bin/perl

use strict;

# Exercise: the user provides the script with two arguments: the name of the fastq file and the length of the reads they want. the script returns how many reads are below a certain length and returns the tile ID of those reads.

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $fastq=$ARGV[0];
my $length=$ARGV[1];


open(my $filehandle, '<', $fastq) || die "Cannot open file!!\n";


my $track=1;
my $count=0;
my $tilenb=0;
my @list=();

while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { @list=split(/:/, $row);
    $tilenb=@list[4];
    $track++;
  }
  elsif($track==2) #it's a sequence
  { my $lengthR=length($row);
    if($lengthR lt $length) # idk why but if < doesnt work use lt
    { $count=$count+1;
      print "$tilenb\n";
    }
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { 
    $track=1;
  }
}

print "There are $count reads less than $length\n";

close $filehandle;
