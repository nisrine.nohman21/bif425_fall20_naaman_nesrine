#!/usr/bin/perl

use strict;

# Exercise: the user gives an argument the SAM file and you output the read with the shortest TLEN.This can be modified to give shortest mapping length by calculating the SEQ instead of TLEN

if($#ARGV+1!=1)
{
 print "You did not enter an argument.\n";
 exit 1;
}

my $SAM=$ARGV[0];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";

my $min;
my $read="";
my $count=1;

while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[H][W][I]/) # is a read
  { my @list=split(/	/, $row);
    my $TLEN=@list[8];
    $TLEN=abs($TLEN);
    if($count==1) #it's the first read encountered
    { $min=$TLEN;
    }
    else
    { if($TLEN<$min)
     { $min=$TLEN;
       $read=$row;
     }
    }
    $count++;
  }
}

print "The shortest TLEN is: $min\n";
print "The read with the shortest TLEN is: $read\n";



close $filehandle;
