#!/usr/bin/perl

use strict;
# Exercise: this script returns the headers of the reads that have the maximum average quality. The maximum average quality score is passed as an argument to the script.

# the first argument is the name of the trimmed file
# The second argument is the maximum average quality score returned by the script: maxQ.pl

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $trimmed=$ARGV[0];
my $maxQ=$ARGV[1];

open(my $filehandle, $trimmed) || die "Cannot open file!!\n";
open (WRITEFILE , ">ReadsMAXQ2.fasta");

my $track=1;
my $header="";

while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { 
    $header=$row;
    $track++;
  }
  elsif($track==2) #it's a sequence
  { 
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { my $quality=$row;
    my $avg=0;
    my $length_of_quality=length($row);
    for(my $i=0;$i<$length_of_quality;$i++)
    { my $char=substr($row, $i, 1);
      $avg=$avg+(ord($char)-33);
    }
    $avg=$avg/$length_of_quality;
   
    if($avg eq $maxQ)
    { if( ! print WRITEFILE "$header\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track=1;
  }
}


close WRITEFILE;
close $filehandle;
