#!/bin/bash

# The pipeline:

# 1: FASTQC

# first, i uploaded the latest version of FASTQC version 0.11.9 on my path on the server then used fastqc command to extract the html file:

time FastQC/fastqc --outdir=./ /mnt/gkhazen/NGS-Fall2020/FinalProject/392_1.fastqc.gz
time FastQC/fastqc --outdir=./ /mnt/gkhazen/NGS-Fall2020/FinalProject/392_2.fastqc.gz

# i used --outdir to specify where i want the output.

# the time it took is: 

# real    6m58.293s
# user    6m55.347s
# sys     0m7.022s
# for the first file 

# real    7m4.985s
# user    7m0.704s
# sys     0m8.479s
# for the second file

# both files have the following number of reads: 30895087


# 2: TRIMMING

# Trimming: we need to remove the adapters.
# The tiles and the qualities of the bases are good and they dont need changing. For this reason, i specified the same arguments as the ones we did used in class: allowing a minimum quality of 3 to keep a leading 5' or a trailing 3', using a window size of 4:15 meaning allowing  minimum average quality of 15 every 4 positions and assigning MINLEN to 36 which is the mimum length of a read acceptable.

time java -jar /usr/src/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 4 -trimlog ./392.log /mnt/gkhazen/NGS-Fall2020/FinalProject/392_1.fastq.gz /mnt/gkhazen/NGS-Fall2020/FinalProject/392_2.fastq.gz ./392_1_paired.fastq.gz ./392_1_unpaired.fastq.gz ./392_2_paired.fastq.gz ./392_2_unpaired.fastq.gz ILLUMINACLIP:/usr/src/Trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

# the time it took:
# real    23m6.640s
# user    87m3.992s
# sys     25m40.084s


# now we re-run fastqc on the output and get the html file again to make sure the adapters are no longer there:

time FastQC/fastqc --outdir=./ ./392_1_paired.fastq.gz
time FastQC/fastqc --outdir=./ ./392_2_paired.fastq.gz

# time for first paired:
# real    6m38.421s
# user    6m35.843s
# sys     0m5.751s

# time for second paired:
# real    6m58.520s
# user    6m56.136s
# sys     0m6.030s

# 3: Downloading the reference chr10.

wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr10.fa.gz

time zcat chr10.fa.gz > chr10.fa

# real    0m1.327s
# user    0m1.201s
# sys     0m0.126s

# 4: indexing chr10:

time bwa index -p chr10bwaidx -a bwtsw chr10.fa

# time taken by indexing:

# real    2m43.678s
# user    2m28.481s
# sys     0m13.505s

# 5: Aligning to the chr10:

time bwa mem -t 8 -R '@RG\tID:rg1\tSM:s392\tPL:illumina\tLB:lib1\tPU:HNLHYDSXX:1:GCCGGACA+TGTAAGAG' chr10bwaidx ./392_1_paired.fastq.gz ./392_2_paired.fastq.gz > ./392_aln.sam


#READ GROUP INFO: here it is '@RG\tID:rg1\tSM:s392\tPL:illumina\tLB:lib1\t:PU:HNLHYDSXX:1:TAAGGCGA'
#Basically it has info regarding:
#RG, ID: read group identifier, SM: sample, name of the sample sequence , PU: platform unit, has flow cell, lane and sample barcode , PL: platform i.e. illumina, solid etc. LB: library id used.
#PL AND PU CAN BE OBTAINED FROM THE HEADER OF THE READS IN THE FASTQ FILES THEMSELVES.

# time of the alignment:

# real    37m17.886s
# user    298m0.575s
# sys     1m53.589s

# 6: Converting SAM to BAM:

time samtools view -Sb 392_aln.sam > 392_aln.bam

# time for conversion:

# real    10m30.348s
# user    9m58.710s
# sys     0m31.193s

# 7: validating the sam/bam file using the gatk tool (not the picard).

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk ValidateSamFile INPUT=392_aln.bam MODE=SUMMARY

# time for the validating:

# real    5m40.430s
# user    8m42.801s
# sys     0m34.825s

# it has been reported that no errors are found in the validation part.

# 8: Sorting the BAM file

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk SortSam INPUT=392_aln.bam OUTPUT=392_sorted.bam SORT_ORDER=coordinate

# time for sorting:

# real    9m39.344s
# user    37m43.826s
# sys     25m54.620s

# 9: Marking duplicates using gatk

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk MarkDuplicates INPUT=392_sorted.bam OUTPUT=392_dedup.bam METRICS_FILE=392.metrics

# time for the marking duplicates:

# real	9m18.334s
# user	181m26.503s
# sys	49m32.774s


# 10: Base Recalibrator:

# first: indexing the reference genome to create a .fa.fai extension index.

samtools faidx ./chr10.fa

# second: creating a sequence dictionary.

/mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk CreateSequenceDictionary R=chr10.fa O=chr10.dict 

# base recalibrator:

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk BaseRecalibrator -I 392_dedup.bam -R chr10.fa --known-sites /mnt/NGSdata/snpdb151_All_20180418.vcf -O recal_data.table

# time for base recalibrator:

# 7m0.221s
# user    10m16.641s
# sys     12m47.215s

# 11: ApplyBQSR

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk ApplyBQSR -R chr10.fa -I 392_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam

# time took the ApplyBQSR:

# real    22m34.492s
# user    31m34.341s
# sys     4m10.016s

# 12: generating a plot summarizing the table: errors occured because the plot function is not working.

# 13: HaplotypeCaller for generating a GVCF (for joint genotyping). GenomicVCF is the intermediate output.

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk --java-options "-Xmx4g" HaplotypeCaller -R chr10.fa -I output_recal.bam -O 392out.g.vcf.gz -ERC GVCF

# time it took:

# real    99m29.496s
# user    146m43.573s
# sys     6m41.884s

# 14: generating the final vcf file using the GenotypeGVCF tool. here is where the joint genotyping happens by passing in the GVCF

time /mnt/gkhazen/NGS-Fall2020/gatk-4.1.9.0/gatk --java-options "-Xmx4g" GenotypeGVCFs -R chr10.fa -V 392out.g.vcf.gz -O 392SNPS.vcf.gz

# time it took for the vcf file:

# real    1m26.577s
# user    2m14.360s
# sys     0m4.249s

# 15: Annotation:

# download the snpeff:

wget https://sourceforge.net/projects/snpeff/files/snpEff_latest_core.zip/download

gunzip 392SNPS.vcf.gz
time java -Xmx4g -jar ./snpEff/snpEff.jar ann -v -stats 392SNPS_stats.html -csvStats 392SNPS_stats.csv hg38 392.vcf > 392_ann.vcf

# time it took:

# real    7m33.199s
# user    14m15.682s
# sys     4m7.295s


