#!/usr/bin/perl

use strict;

# Exercise: given read, use the CIGAR to output how many genome parts the read maps to. using SAM file.
# Arguments to be given: the SAM file and the header of the read.
# BE CAREFUL, THIS MIGHT RETURN MORE THAN ONE ANSWER IF THE READ WANTED HAS A PAIRED ONE OR SUPPLEMENTARY ONES OR SECONDARY ONES SO PAY ATTENTION TO WHAT HE WANTS AND USE BITFLAGS IF IT'S NECESSARY

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments. Please enter the name of the SAM file as the first argument and the header of the read as the second argument. Make sure to enter the header put in \"\"\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $header=$ARGV[1];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";
my @list=();

while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/$header\t/) # is the read that we want.
  { #print "entered\n";
    @list=split(/	/, $row);
    my $CIGAR=@list[5];
    @list=grep length, split /(\d+)/, $CIGAR;
  }
}


my $sum=0;
my $length=@list;
print "$length\n";
for(my $i=0;$i<$length;$i=$i+2)
{ #print "entered the loop\n";
  my $nb=@list[$i];
  $nb=int($nb);
  if(@list[$i+1] ne 'D' && @list[$i+1] ne 'P' && @list[$i+1] ne 'H' && @list[$i+1] ne 'N')
  { $sum+=$nb;
  }
}

print "The length of the read you entered is: $sum\n";


close $filehandle;
