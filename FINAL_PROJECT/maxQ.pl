#!/usr/bin/perl

use strict;
# Exercise: this script returns the maximum average quality among the reads.

# the first argument is the name of the trimmed file

if($#ARGV+1!=1)
{
 print "You did not enter the name of the trimmed file.\n";
 exit 1;
}

my $trimmed=$ARGV[0];

open(my $filehandle, $trimmed) || die "Cannot open file!!\n";


my $maxq=0;
my $track=1;


while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { 
    $track++;
  }
  elsif($track==2) #it's a sequence
  { 
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { my $quality=$row;
    my $avg=0;
    my $length_of_quality=length($row);
    for(my $i=0;$i<$length_of_quality;$i++)
    { my $char=substr($row, $i, 1);
      $avg=$avg+(ord($char)-33);
    }
    $avg=$avg/$length_of_quality;
    
    if($avg>=$maxq)
    { $maxq=$avg;
    }
    $track=1;
  }
}

print "The maximum quality score among the reads is: $maxq\n";

close $filehandle;
