#!/usr/bin/perl

use strict;

# Exercise: the user provides the script with two arguments: the name of the fastq file and the number of the tile whose reads we want to remove. the script removes the reads with the specified tile number.

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $fastq=$ARGV[0];
my $tilenb=$ARGV[1];

open(my $filehandle, '<', $fastq) || die "Cannot open file!!\n";
open (my WRITEFILE , ">output.fastq");

my $track=1;
my $print=0;

while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { if($row!~/.*:.*:.*:.*:$tilenb:.*/)
    { $print=1;
      if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track++;
  }
  elsif($track==2) #it's a sequence
  { if($print==1)
    { if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track++;
  }
  elsif($track==3) #it's a +
  { if($print==1)
    { if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track++;
  }
  else # it's a quality score
  { if($print==1)
    { if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track=1;
    $print=0;
  }
}

close WRITEFILE;
close $filehandle;
