#!/usr/bin/perl

use strict;

# Exercise: user enters as an argument a vcf file and this script writes to another file the SNPs that have depth>30

if($#ARGV+1!=1)
{
 print "You did not enter an argument!\n";
 exit 1;
}

my $VCF=$ARGV[0];
my $count=0;
my $depth=0;

open(my $filehandle, '<', $VCF) || die "Cannot open file!!\n";
open (WRITEFILE , ">output.vcf");

while(my $row=<$filehandle>)
{ chomp $row;
  if($row =~/^#/) # is a header
  { if( ! print WRITEFILE "$row\n" )
    { warn "you cant write to the file! ERROR: $!\n"; }
  }
  else # is a SNP
  { my @list=split(/\t/, $row);
    my $INFO=@list[7];
    my @list2=split(/;/, $INFO);
    my $length=@list2;
    for(my $i=0;$i<$length;$i++)
    { if(@list2[$i]=~/^DP/)
      { my @list3=split(/=/, @list2[$i]);
        $depth=@list3[1];
      }
    }
    if($depth>30)
    { if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
  }
}



close WRITEFILE;
close $filehandle;
