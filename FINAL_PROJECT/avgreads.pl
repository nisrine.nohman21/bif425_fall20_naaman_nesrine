#!/usr/bin/perl

use strict;

# Exercise: we pass the mapped sam file and this returns the avg mapping quality of all mapped reads.

if($#ARGV+1!=1)
{
 print "You did not enter the sam file!\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $avg=0;
my $sum=0;
my $count=0;

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[A][0][0][7][2][1]/) # is the read that we want and not the header which starts with @SQ etc..
  { my @list=split(/	/, $row);
    my $MAPQ=@list[4];
    $sum=$sum+$MAPQ;
    $count++;
  }
}

$avg=$sum/$count;

print "The average mapping quality for all the mapped reads is: $avg\n";


close $filehandle;

