#!/usr/bin/perl

use strict;
# Exercise: this script returns the longest and the shortest reads and their respective average quality scores.

# the first argument is the name of the trimmed file

if($#ARGV+1!=1)
{
 print "You did not enter 1 argument.\n";
 exit 1;
}

my $trimmed=$ARGV[0];

open(my $filehandle, '<', $trimmed) || die "Cannot open file!!\n";
open(my $filehandle2, '<', $trimmed) || die "Cannot open file!!\n";


my $track1=1;
my $track2=1;
my $min=0;
my $max=0;
my $qmin=0;
my $qmax=0;
my $length=0;
my $minread="";
my $maxread="";
my $changedmin=0;
my $changedmax=0;

while(my $row=<$filehandle>)
{ chomp $row;
  if($track1==1) # it's a header
  { $track1++;
  }
  else # it's a sequence
  { $min=length($row);
    $track1++;
  }
}

while(my $row=<$filehandle2>)
{ chomp $row;
  if($track2==1) #it's a header
  { 
     $track2++;
  }
  elsif($track2==2) #it's a sequence
  { #print "$row\n";
    $length=length($row);
    if($length<$min)
    { $min=$length;
      $minread=$row;
      $changedmin=1;
    }
    if($length>$max)
    { $max=$length;
      $maxread=$row;
     $changedmax=1;
    }
    $track2++;
  }
  elsif($track2==3) #it's a +
  { 
    $track2++;
  }
  else # it's a quality score
  { my $avg=0;
    my $length_of_quality=length($row);
    for(my $i=0;$i<$length_of_quality;$i++)
    { my $char=substr($row, $i, 1);
      $avg=$avg+(ord($char)-33);
    }
    $avg=$avg/$length_of_quality;
    
    if($changedmin==1)
    { $qmin=$avg;
      $changedmin=0;
    }
    if($changedmax==1)
    { $qmax=$avg;
      $changedmax=0;
    }
    $track2=1;
  }
}

print "The read having the maximum length is: $maxread\n";
print "The quality score of that read is: $qmax\n";
print "The read having the minimum length is: $minread\n";
print "The quality score of that read is: $qmin\n";

close $filehandle;
close $filehandle2;
