#!/usr/bin/perl

use strict;
# Exercise: this script returns the avg of quality of first 3 bps and last 3 bps of a particular read and the difference of their qualities.

# the first argument is the name of the trimmed file
# the second argument is the ID of the read

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $trimmed=$ARGV[0];
my $readID=$ARGV[1];

open(my $filehandle, '<', $trimmed) || die "Cannot open file!!\n";


my $track=1;
my $true=0;
my $avg1=0;
my $avg2=0;

while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  {  if($row eq $readID)
     { $true=1;
     }
     $track++;
  }
  elsif($track==2) #it's a sequence
  { 
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { if($true==1) #this is the quality of the read we are looking for
    { 
      my $length_of_quality=length($row);
      for(my $i=0;$i<3;$i++)
      { my $char=substr($row, $i, 1);
        $avg1=$avg1+(ord($char)-33);
      }
      $avg1=$avg1/3;
      for(my $i=$length_of_quality-1;$i>$length_of_quality-4;$i--)
      { my $char=substr($row, $i, 1);
        $avg2=$avg2+(ord($char)-33);
      }
      $avg2=$avg2/3;
      $true=0;
    } 
    $track=1;
  }
}

my $diff=$avg1-$avg2;
$diff=abs($diff);

print "The avg quality score for the first 3 bps 5' is: $avg1\n";
print "The avg quality score for the last 3 bps 3' is: $avg2\n";
print "The difference between them is: $diff\n";

close $filehandle;
