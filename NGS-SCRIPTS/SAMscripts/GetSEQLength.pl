#!/usr/bin/perl

use strict;

# Exercise: given read, use the SEQ to output how many genome parts the read maps to. using SAM file.
# Arguments to be given: the SAM file and the header of the read.
# BE CAREFUL, THIS MIGHT RETURN MORE THAN ONE ANSWER IF THE READ WANTED HAS A PAIRED ONE OR SUPPLEMENTARY ONES OR SECONDARY ONES SO PAY ATTENTION TO WHAT HE WANTS AND USE BITFLAGS IF IT'S NECESSARY

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments. Please enter the name of the SAM file as the first argument and the header of the read as the second argument. Make sure to enter the header put in \"\"\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $header=$ARGV[1];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/$header\t/) # is the read that we want.
  { print "entered\n";
    my @list=split(/	/, $row);
    my $SEQ=@list[9];
    my $length=length($SEQ);
    print "The given read maps to $length parts of the genome\n";
  }
}

close $filehandle;
