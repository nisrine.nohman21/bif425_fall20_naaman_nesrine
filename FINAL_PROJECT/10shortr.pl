#!/usr/bin/perl

use strict;
# Exercise: this script returns the header of the read that has the maximum average quality along with this maximum quality.

# the first argument is the name of the trimmed file

if($#ARGV+1!=1)
{
 print "You did not enter the name of the trimmed file.\n";
 exit 1;
}

my $trimmed=$ARGV[0];

open(my $filehandle, '<', $trimmed) || die "Cannot open file!!\n";

my $header="";
my $track=1;
my $count=0;
my $list=();
my %hash=();


while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { 
    $header=$row; 
    $track++;
  }
  elsif($track==2) #it's a sequence
  { 
    my $length=length($row);
    if($count==0)
    { @list=(@list , $length);
      $hash{$length}="$header";
      @list= sort { $a <=> $b } @list;
      $count++;
    }
    else
    { my $size=@list;
      if($length<@list[$size-1])
      { if($count!=10)
        { @list=(@list , $length);
          $hash{$length}="$header";
          @list= sort { $a <=> $b } @list;
          $count++;
        }
        else
        {  $keyrm=@list[$size-1];
           @list[$size-1]=$length;
           $hash{$length}="$header";
           @list= sort { $a <=> $b } @list;
           delete $hash{$keyrm};
        }    
      }
    }
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { 
    $track=1;
  }
}

print "The 10 shortest read ID along with their lengths are:\n";
for (keys %hash) # No variable is specified in the for loop before the (keys %hash) so the default variable is taken. we're looping through the list of keys so $_ will be each key of the list and $hash{$_} will be the value of that key
{ print "$hash{$_} of length $_\n"; }

close $filehandle;
