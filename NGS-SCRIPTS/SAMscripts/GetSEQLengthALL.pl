#!/usr/bin/perl

use strict;

# Exercise: use the SEQ to output how many genome parts each read in the SAM file maps to. using SAM file.
# Arguments to be given: the SAM file.

if($#ARGV+1!=1)
{
 print "You did not enter 1 arguments.\n";
 exit 1;
}

my $SAM=$ARGV[0];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";
open (WRITEFILE , ">output.txt");

# awk -F "," ' { print $2 } ' filename.txt

while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[H][W][I]/) # is the read that we want.
  { my @list=split(/	/, $row);
    my $SEQ=@list[9];
    my $length=length($SEQ);
    if( ! print WRITEFILE "$length\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
  }
}

close $filehandle;
close WRITEFILE;
