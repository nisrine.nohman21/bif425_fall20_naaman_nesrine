#!/usr/bin/perl

use strict;
# Exercise: output the number of reads in the trimmed file that are of certain length, and return the average of the quality score of each read having that length. the length is also provided by the user.

# the first argument is the name of the trimmed file
# the second argument is the length we want

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $trimmed=$ARGV[0];
my $length=$ARGV[1];
#print "$length\n";

open (FILEHANDLE , $trimmed) || die "The file failed to open!\n";

my @reads=();
my @qualities=();
my $nextisscore=0;
my $getscore=0;

while (defined(my $line=<FILEHANDLE>))
{ chomp $line;
  print "$line\n";
  if($line=~/^@[H][W][I]/)
  { $nextisscore=0;
  }
  elsif($line=~/^\+/)
  { $nextisscore=1;
  }
  else
  { if($nextisscore==0)
    { my $read=$line;
      my $lengthh=length($read);
      if($lengthh==$length)
      { @reads=(@reads,$read); 
        $getscore=1;
      }
    }
    else
    { 
      if($getscore==1)
      { my $quality=$line;
        my $avg=0;
        my $length_of_quality=length($quality);
        for(my $i=0;$i<$length_of_quality;$i++)
        { my $char=substr($quality, $i, 1);
          $avg=$avg+(ord($char)-33);
        }
        $avg=$avg/$length_of_quality;
        @qualities=(@qualities,$avg);
        $getscore=0;
      }
    }
  }
}

my $size=@reads;

print "There are $size reads with a length of $length and the average quality score of each read is:\n\n";

for (my $x=0; $x<$size; $x++)
{ print "Read: @reads[$x]\n";
  print "Average quality: @qualities[$x]\n";
  print "\n"; }


close (FILEHANDLE);





