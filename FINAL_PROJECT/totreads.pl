#!/usr/bin/perl

use strict;

# Exercise: Use the CIGAR string, to compute the number of reads without any Insertion or Deletion


if($#ARGV+1!=1)
{
 print "You did not enter an argument. Please enter the name of the SAM file\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $count=0;

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[A][0][0][7][2][1]/) # is the reads that we want and not the header which starts with @SQ etc..
  { $count++; 
  }
}


print "The number of reads is: $count\n";


close $filehandle;
