#!/usr/bin/perl

use strict;

# Exercise: user enters as an argument a vcf file and this script outputs how many SNPs pass the filter.

if($#ARGV+1!=1)
{
 print "You did not enter an argument!\n";
 exit 1;
}

my $VCF=$ARGV[0];
my $count=0;

open(my $filehandle, '<', $VCF) || die "Cannot open file!!\n";

while(my $row=<$filehandle>)
{ chomp $row;
  if($row!~/^#/) # is a SNP
  { my @list=split(/\t/, $row);
    my $FILTER=@list[6];
    if($FILTER eq "PASS")
    { $count++;
    }
  }
}

print "There are $count SNPS that pass the filter in the vcf file you provided.\n";




close $filehandle;
