#!/usr/bin/perl

use strict;

# Exercise: The user enters a sam file. This script outputs to another file all the reads that have a complementary and to another file the unique reads that dont have a complementary.
# BE CAREFUL, THIS OUTPUTS THE COMPEMENTARY READS ALL OF THEM NOT ONLY ONE OF THE 2 COMPLEMENTARY.

if($#ARGV+1!=1)
{
 print "You did not enter an argument.\n";
 exit 1;
}

my $SAM=$ARGV[0];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";
open (WRITEFILE , ">complementaryReads.txt");
open (FILEHANDLE , ">uniqueReads.txt");


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[H][W][I]/) # is the read that we want and not the header which starts with @SQ etc..
  { my @list=split(/	/, $row);
    my $RNEXT=@list[6];
    if($RNEXT eq "=") # it has a pair
    { if( ! print WRITEFILE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    else
    { if( ! print FILEHANDLE "$row\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    } 
  }
}

close WRITEFILE;
close WRITEFILE2;
close $filehandle;

