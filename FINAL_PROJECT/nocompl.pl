#!/usr/bin/perl

use strict;

# Exercise: return the Number of reads without a pair complement.

if($#ARGV+1!=1)
{
 print "You did not enter the sam file!\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $count=0;

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[A][0][0][7][2][1]/) # is the read that we want and not the header which starts with @SQ etc..
  { my @list=split(/	/, $row);
    my $RNEXT=@list[6];
    if($RNEXT eq "*") # it has a pair
    { $count++;
    }  
  }
}


print "The number of reads without a complement is: $count\n";


close $filehandle;

