#!/usr/bin/perl

use strict;

# Exercise: user enters as an argument a vcf file and this script outputs how many dbSNPs we have in the file.

if($#ARGV+1!=1)
{
 print "You did not enter an argument!\n";
 exit 1;
}

my $VCF=$ARGV[0];
my $count=0;

open(my $filehandle, '<', $VCF) || die "Cannot open file!!\n";

while(my $row=<$filehandle>)
{ chomp $row;
  if($row!~/^#/) # is a SNP
  { my @list=split(/\t/, $row);
    my $ID=@list[2];
    my $INFO=@list[7];
    if($ID=~/^[r][s]/ || $INFO=~/.*DB.*/)
    { $count++;
    }
  }
}

print "There are $count SNPS in the vcf file you provided.\n";




close $filehandle;
