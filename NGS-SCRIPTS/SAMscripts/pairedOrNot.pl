#!/usr/bin/perl

use strict;

# Exercise: the user gives an argument the SAM file and you output whether the raw read file is paired or not.

if($#ARGV+1!=1)
{
 print "You did not enter an argument.\n";
 exit 1;
}

my $SAM=$ARGV[0];

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";

my $ispaired=0;

while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[H][W][I]/) # is a read
  { my @list=split(/	/, $row);
    my $RNEXT=@list[6];
    if($RNEXT eq "=")
    { $ispaired=1; }
  }
}

if($ispaired==0)
{ print "This is a single ended read\n"; }
else
{ print "This is a paired end read\n"; }
  


close $filehandle;
