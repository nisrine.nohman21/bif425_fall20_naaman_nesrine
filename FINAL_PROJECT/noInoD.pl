#!/usr/bin/perl

use strict;

# Exercise: Use the CIGAR string, to compute the number of reads without any Insertion or Deletion


if($#ARGV+1!=1)
{
 print "You did not enter an argument. Please enter the name of the SAM file\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $count=0;
my $ID=0;

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";
my @list=();
my @list2=();

while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[A][0][0][7][2][1]/) # is the reads that we want and not the header which starts with @SQ etc..
  { 
    @list=split(/	/, $row);
    my $CIGAR=@list[5];
    if($CIGAR eq "*")
    {
      next; }
    @list2=grep length, split /(\d+)/, $CIGAR;
    
  my $size=@list2;
  for(my $i=1;$i<$size;$i=$i+2)
  { my $letter=@list2[$i];
    if($letter eq 'D' || $letter eq 'I')
    { $ID=1;
    }
  }  
  if($ID==0)
  { $count++;
    $ID=0; }
  else
  { $ID=0; }
}
}


print "The number of reads without any insertions or deletions is: $count\n";


close $filehandle;
