#!/usr/bin/perl

use strict;
# Exercise: this script writes to a file the headers of the reads that have a length of 36.

# the first argument is the name of the trimmed file

if($#ARGV+1!=1)
{
 print "You did not enter the name of the trimmed file.\n";
 exit 1;
}

my $trimmed=$ARGV[0];

open(my $filehandle, '<', $trimmed) || die "Cannot open file!!\n";
open (WRITEFILE , ">shortestReads.fasta");

my $header="";
my $track=1;
my $count=0;
my $list=();
my %hash=();


while(my $row=<$filehandle>)
{ chomp $row;
  if($track==1) #it's a header
  { 
    $header=$row; 
    $track++;
  }
  elsif($track==2) #it's a sequence
  { 
    my $length=length($row);
    if($length==36)
    { if( ! print WRITEFILE "$header\n" )
      { warn "you cant write to the file! ERROR: $!\n"; }
    }
    $track++;
  }
  elsif($track==3) #it's a +
  { 
    $track++;
  }
  else # it's a quality score
  { 
    $track=1;
  }
}

close WRITEFILE;
close $filehandle;
