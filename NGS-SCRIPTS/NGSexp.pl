# fastq trimmed file format:

'@HWI-D00119:50:H7AP8ADXX:1:1101:1242:2178 1:N:0:TAAGGCGA
ACATAGTGGTCTGTCTTCTGTTTATTACAGTACCTGTAATAATTCTTGATGCTGTCCAGACTTGGAATATTGAATGCATCTAATTTAAAAAAAAAAAAAAA
+
@@@DDBAAB=AACEEEFEEAHFEEECHAH>CH4A<+:CF<FFE<<DF@BFB9?4?<*?9D;DCFCEEFIII<=FFF4=FDEECDFIFFEECBCBBBBBB>5'

first line is the header and it has:


unique instrument name:HWI-D00119
Run Id:50
Flow cell ID:H7AP8ADXX
Flowcell lane:1 ( we have around 8 lanes and each lane has two columns each column has up to 50 tiles)
Tile nb:1101 (1 lane nb 1 col nb 01 tile nb)
X and Y coordinate within the tile:1242:2178
The member of a pair(1 if unpaired or paired and its the first member of the pair and 2 if paired end and second member): 1
Y if the read if filtered and N if not: N
0 when none of the control bits are on otherwise its pair: 0
Index sequence: TAAGGCGA


ASCII=phred offset score(33 or 64) + quality score
quality score= ASCII-phred offset

SAM=sequence alignment map format

ssh -Y nesrine.naaman@linuxdev.accbyblos.lau.edu.lb

scp /pathToFileYouWannaCopyToServer nesrine.naaman@linuxdev.accbyblos.lau.edu.lb:path/on/server

scp username@source:/location/to/file username@destination:/where/to/put

SAM:

1) QNAME: query template name i.e. read name
2) FLAG
3) RNAME: reference sequence name
4) POS: position of the first mapped base of the read to the left
5) MAPQ: Mapping Quality
6) CIGAR
7) RNEXT: reference name of the next read or pair --> "=" if paired "*" if single end
8) PNEXT: position of the pair of the read we are at. BE CAREFUL THIS IS THE POSITION OF THE LEFTMOST BP OF THE PAIRED READ THAT MAPPED TO THE REFERENCE AND NOT THE RIGHTMOST BP. PNEXT-POS+nbrOfNtsOfPairedReadR2=TLEN
9) TLEN: observed template length: nbr of bps at the start of where r1 mapped till the end of where r1 paired is mapping
10) SEQ: This is the segment sequence 
11) QUAL: ASCii of bps

CIGAR:

M:match mismatch
I:Insertion
D:Deletion
N:skipped from reference
S:Soft clipping
H:Hard clipping
P:Padding
=:match
X:mismatch

we dont count D,H,P,N in SEQ

VCF format:

##fileformat=VCFv4.0
##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">
##INFO=<ID=HM2,Number=0,Type=Flag,Description="HapMap2 membership">
##INFO=<ID=HM3,Number=0,Type=Flag,Description="HapMap3 membership">
##INFO=<ID=AA,Number=1,Type=String,Description="Ancestral Allele, ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/pilot_data/technical/reference/ancestral_alignments/README">
##reference=human_b36_both.fasta
##INFO=<ID=AC,Number=1,Type=Integer,Description="total number of alternate alleles in called genotypes">
##INFO=<ID=AN,Number=1,Type=Integer,Description="total number of alleles in called genotypes">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth from MOSAIK BAM">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
1	1105366	.	T	C	.	PASS	AA=T;AC=4;AN=114;DP=3251
1	1105411	.	G	A	.	PASS	AA=G;AC=1;AN=106;DP=2676


