#!/usr/bin/perl

use strict;


if($#ARGV+1!=1)
{
 print "You did not enter an argument. Please enter the name of the SAM file\n";
 exit 1;
}

my $VCF=$ARGV[0];

my $wt=0;
my $mut=0;
my $hetero=0;
my @array=();
my $string=0;
my $before=0;
my $after=0;

open(my $filehandle, '<', $VCF) || die "Cannot open file!!\n";

while(my $row=<$filehandle>)
{ chomp $row;
  if (!($row=~/^#/))
  {  
     @array=split(/\t/, $row);
     $string = @array[9]; 
     for(my $i=0; $i<length($string); $i++)
     {   
	if (substr($string, $i+1,1) eq "|" || substr($string, $i+1,1) eq "/")
	{
	  $before= substr($string, $i,1);
	  $after= substr($string, $i+2,1);
	  if ($before eq $after)
	  {
	    if ($before eq "0") 
	    {
		$wt++;
	    }
            else 
	     {
		$mut++;
	      }
           }
         
	  else 
	  {
	     $hetero++; }
         }
	
       $i=length($string);
     }
   }
}

		
	
print "homozygotes wild type: $wt\n";
print "homozygotes mutant: $mut\n";
print "heterozygotes: $hetero\n";
close $filehandle;
