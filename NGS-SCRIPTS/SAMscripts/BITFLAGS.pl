#!/usr/bin/perl

use strict;

# To return reads or the number of reads with certain attributes from the FLAGS column in the SAM/BAM file, we use the SAMTOOLS commands:

# on the terminal we run using the BAM OR the SAM.

samtools view -f 0X800 NIST7035_aln.bam 
# This outputs the reads that are supplementary. (it basically outputs the reads whose bits in the FLAG option are there in the 0X800. so if 0X800 has 1 at the bit 2^11 all the reads that also have 1 at 2^11 will be returned).

samtools view -f 0X100 NIST7035_aln.bam
# This outputs the reads that are supplementary

samtools view -F 0X100 NIST7035_aln.bam
# This outputs the reads that are NOT supplementary 

samtools view -F 0X900 NIST7035_aln.bam
# This outputs the reads that are neither supplementary nor secondary --> reads that are primary. 
# 0X900=0X800+0X100.

samtools view -f 0X900 NIST7035_aln.bam
# This outputs reads that have FLAG and 0X900 == TRUE i.e. 1 at 0X800 and 1 at 0X100 which is impossible

samtools view -c -f 0X200 NIST7035_aln.bam
# This outputs THE NUMBER of reads that do not pass quality controls.

samtools view -c -f 0X5 NIST7035_aln.bam
# This outputs the number of reads that have multiple segments and that are unmapped.

samtools view -c -f 0X4 NIST7035_aln.bam
# outputs the nb of unmapped reads

samtools view -c -F 0X4 NIST7035_aln.bam
# outputs the nb of mapped reads

# NOTE: IF YOU WANT TO OUTPUT THE READS AND REDIRECT THEM TO A NEW FILE ASK IF U DO IT WITH A BAM OR A SAM. BE CAREFUL

# Bitflags:

2^0 0x1 1: Template having multiple segments in sequencing
2^1 0x2 2: each segment properly aligned according to the aligner
2^2 0x4 4: segment unmapped
2^3 0x8 8: next segment in the template unmapped
2^4 0x10 16: SEQ being reverse complemented
2^5 0x20 32: SEQ of the next segment in the template being reverse complimented
2^6 0x40 64: The first segment in the template
2^7 0x80 128: The last segment in the template
2^8 0x100 256: secondary alignment
2^9 0x200 512: not passing quality controls
2^10 0x400 1024: PCR or optical duplicate
2^11 0x800 2048: supplementary alignment

0X900 = 0X800 + 0X100 i.e. we have 1 at 0X800 and 1 at 0X100

99 --> 1 at 2^6 , 1 at 2^5, 1 at 2^1 and 1 at 2^0
147 --> 1 at 2^0, 1 at 2^1, 1 at 16 and 1 at 128
83 --> 1 at 1, 1 at 2, 1 at 16 and 1 at 64
163 --> 1 at 1 , 1 at 2, 1 at 32 and 1 at 128
