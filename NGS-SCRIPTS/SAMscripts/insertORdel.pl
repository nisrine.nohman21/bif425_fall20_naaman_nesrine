#!/usr/bin/perl

use strict;

# Exercise: The user enters a sam file and the header of two paired reads. This script outputs whether there has been an insertion or a deletion between them.

if($#ARGV+1!=2)
{
 print "You did not enter 2 arguments.\n";
 exit 1;
}

my $SAM=$ARGV[0];
my $H=$ARGV[1];
my $r1=0;
my $l1=0;
my $POS=0;
my $PNEXT=0;
my $c1=0;

open(my $filehandle, '<', $SAM) || die "Cannot open file!!\n";


while(my $row=<$filehandle>)
{ chomp $row;
  if($row=~/^[H][W][I]/) # is the read that we want and not the header which starts with @SQ etc.. or we do if($row=~/^\@/){next;}
  { my @list=split(/	/, $row);
    my $header=@list[0];
    if($header eq $H)
    { my $RNEXT=@list[6];
      if($RNEXT eq "=")
      {if($r1==0)
      {
      my $SEQ=@list[9];
      $l1=length($SEQ);
      $POS=@list[3];
      $c1=$l1+$POS;
      $PNEXT=@list[7];
      $r1=1;
      last;
      }
      }
    }
  }
}

if($c1<$PNEXT)
{ print "There has been an insertion \n";
}
else
{ print "There has been a deletion \n";
}


close $filehandle;

